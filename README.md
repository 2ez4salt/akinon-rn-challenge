# AKINON REACT NATIVE CHALLENGE

> Hi! I'm Talha. Here is my project repo for Akinon Job Interview.

## Installation

> In project main directory

     npm i

> In ios/ directory

     pod deintegrate && pod install

[react-native-gesture-handler installation for android](https://docs.swmansion.com/react-native-gesture-handler/docs/#android)

[re-animated installation for android](https://docs.swmansion.com/react-native-reanimated/docs/installation#android)

## Running

> For iOS

     yarn ios

> For Android

    yarn android

## Testing

    yarn test

## Summary

> In this project, I tried to correctly apply the items requested from me in the report. I hope you will like it. I would be very glad if you indicate to me the parts that you don't like or that you think could be better.
