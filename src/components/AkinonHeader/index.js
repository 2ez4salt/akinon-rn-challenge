import React from "react";
import { View, Image, Text } from "react-native";
import { styles } from "./styles";

export const AkinonHeader = () => {
  return (
    <View style={styles.container}>
      <Image
        style={styles.logo}
        source={require("../../assets/akinon.png")}
      ></Image>
      <Text>Akinon RN Challenge</Text>
    </View>
  );
};
