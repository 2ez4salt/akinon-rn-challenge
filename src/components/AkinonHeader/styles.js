import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  logo: {
    width: 100,
    height: 100,
  },

  container: {
    marginBottom: 15,
    alignItems: "center",
  },
});
