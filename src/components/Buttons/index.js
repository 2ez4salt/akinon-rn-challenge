import React from "react";
import { View, StyleSheet, Text } from "react-native";
import { useDispatch } from "react-redux";
import {
  widthPlus,
  widthMinus,
  heightMinus,
  heightPlus,
  resetSize,
} from "../../store/actions";

import { styles } from "./styles";

export const Buttons = () => {
  const dispatch = useDispatch();
  return (
    <View style={styles.container}>
      <Text style={styles.button} onPress={() => dispatch(widthPlus())}>
        WIDTH +
      </Text>
      <Text style={styles.button} onPress={() => dispatch(widthMinus())}>
        WIDTH -
      </Text>
      <Text style={styles.button} onPress={() => dispatch(heightPlus())}>
        HEIGHT +
      </Text>
      <Text style={styles.button} onPress={() => dispatch(heightMinus())}>
        HEIGHT -
      </Text>
      <Text style={styles.button} onPress={() => dispatch(resetSize())}>
        RESET
      </Text>
    </View>
  );
};
