import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  button: {
    borderColor: "black",
    borderWidth: 2,
    borderStyle: "solid",
    padding: 5,
    margin: 3,
  },
});
