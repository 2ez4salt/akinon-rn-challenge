import React, { useRef } from "react";
import { View, Text, PanResponder, Animated } from "react-native";
import Swipeable from "react-native-gesture-handler/Swipeable";
import { useSelector, useDispatch } from "react-redux";
import { nextImage, previousImage } from "../../store/actions";
import { SwipingImage } from "../Image";
import { styles } from "./styles";

export const Swiper = () => {
  const dispatch = useDispatch();
  const data = useSelector((state) => state.data);
  const count = useSelector((state) => state.counter);
  const width = useSelector((state) => state.width);
  const pan = useRef(new Animated.ValueXY()).current;

  const panResponder = useRef(
    PanResponder.create({
      onMoveShouldSetPanResponder: () => true,
      onPanResponderGrant: () => {
        pan.setOffset({
          x: pan.x._value,
          y: pan.y._value,
        });
      },
      onPanResponderMove: Animated.event([null, { dx: pan.x, dy: pan.y }]),
      onPanResponderRelease: () => {
        pan.flattenOffset();
      },
    })
  ).current;

  return (
    <View style={styles.container}>
      <Animated.View
        style={{
          transform: [{ translateX: pan.x }, { translateY: pan.y }],
        }}
        {...panResponder.panHandlers}
      >
        <View style={[styles.ddcontainer, { width: width }]}>
          <Text>Drag Drop</Text>
        </View>
        <Swipeable
          onSwipeableRightWillOpen={() => dispatch(nextImage())}
          onSwipeableLeftWillOpen={() => dispatch(previousImage())}
          renderLeftActions={() => swipe(count, data)}
          renderRightActions={() => swipe(count, data)}
        >
          <SwipingImage count={count} data={data}></SwipingImage>
        </Swipeable>
      </Animated.View>
    </View>
  );
};

const swipe = (count, data) => {
  return (
    <View>
      <SwipingImage count={count} data={data}></SwipingImage>
    </View>
  );
};
