import React from "react";
import { View, Image } from "react-native";
import { useSelector, useDispatch } from "react-redux";

export const SwipingImage = (props) => {
  const width = useSelector((state) => state.width);
  const height = useSelector((state) => state.height);

  return (
    <View>
      <Image
        style={{ width: width, height: height }}
        source={{ uri: props.data[props.count] }}
      ></Image>
    </View>
  );
};
