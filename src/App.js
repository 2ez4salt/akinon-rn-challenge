import React from "react";
import { View, StyleSheet, SafeAreaView } from "react-native";
import { Provider } from "react-redux";
import { AkinonHeader } from "./components/AkinonHeader";
import { Buttons } from "./components/Buttons";
import { Swiper } from "./components/ImageSwiper";
import { store } from "./store/store";

export default function App() {
  return (
    <Provider store={store}>
      <View style={styles.container}>
        <SafeAreaView>
          <AkinonHeader />
          <Buttons />
          <Swiper />
        </SafeAreaView>
      </View>
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
  },
});
