export const HTTP_STATUS = 200;

export const api_1 = [
  { url: "https://via.placeholder.com/800x600/BD463C" },
  { url: "https://via.placeholder.com/800x600/7A2E27" },
  { url: "https://via.placeholder.com/800x600/FA5E50" },
  { url: "https://via.placeholder.com/800x600/3B1613" },
  { url: "https://via.placeholder.com/800x600/E05548" },
];
export const api_2 = {
  images: [
    { link: "https://via.placeholder.com/600x1000/468DBD" },
    { link: "https://via.placeholder.com/600x1000/2D5C7A" },
    { link: "https://via.placeholder.com/600x1000/5CBBFA" },
    { link: "https://via.placeholder.com/600x1000/162C3B" },
    { link: "https://via.placeholder.com/600x1000/53A8E0" },
  ],
};
export const api_3 = {
  swipers: [
    {
      swiper: {
        image_set: [
          { image_url: "https://via.placeholder.com/800x800/8EBD37" },
          { image_url: "https://via.placeholder.com/800x800/5C7A23" },
          { image_url: "https://via.placeholder.com/800x800/BCFA48" },
          { image_url: "https://via.placeholder.com/800x800/2C3B11" },
          { image_url: "https://via.placeholder.com/800x800/A9E042" },
        ],
      },
    },
  ],
};
