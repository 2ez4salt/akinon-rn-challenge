import * as data from "./../data";
import * as config from "./../../integration-config.json";

export function fetchData() {
  if (data.HTTP_STATUS == 200) {
    const f = new Function(
      config[config.choosen].arguments,
      config[config.choosen].body
    );

    return f(data[config.choosen]);
  }
}
