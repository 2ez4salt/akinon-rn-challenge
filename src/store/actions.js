import {
  NEXT,
  PREVIOUS,
  WIDTH_PLUS,
  WIDTH_MINUS,
  HEIGHT_MINUS,
  HEIGHT_PLUS,
  RESET_SIZE,
} from "./actionTypes";
export const nextImage = () => ({
  type: NEXT,
});

export const previousImage = () => ({
  type: PREVIOUS,
});

export const widthPlus = () => ({
  type: WIDTH_PLUS,
});
export const widthMinus = () => ({
  type: WIDTH_MINUS,
});
export const heightPlus = () => ({
  type: HEIGHT_PLUS,
});
export const heightMinus = () => ({
  type: HEIGHT_MINUS,
});
export const resetSize = () => ({
  type: RESET_SIZE,
});
