import {
  NEXT,
  PREVIOUS,
  WIDTH_PLUS,
  WIDTH_MINUS,
  HEIGHT_MINUS,
  HEIGHT_PLUS,
  RESET_SIZE,
} from "./actionTypes";
import * as helpers from "../helpers/";

const initialState = {
  data: helpers.fetchData(),
  counter: 0,
  width: 300,
  height: 300,
};

export const mainReducer = (state = initialState, action) => {
  switch (action.type) {
    case NEXT:
      if (state.counter == state.data.length - 1) {
        return { ...state, counter: 0 };
      } else {
        return { ...state, counter: state.counter + 1 };
      }
    case PREVIOUS:
      if (state.counter == 0) {
        return { ...state, counter: 4 };
      } else {
        return { ...state, counter: state.counter - 1 };
      }
    case WIDTH_PLUS:
      return { ...state, width: (state.width += 20) };
    case WIDTH_MINUS:
      if (state.width == 80) {
        return { ...state, width: (state.width = 200) };
      } else {
        return { ...state, width: (state.width -= 20) };
      }
    case HEIGHT_PLUS:
      return { ...state, height: (state.height += 20) };
    case HEIGHT_MINUS:
      if (state.height == 60) {
        return { ...state, height: (state.height = 200) };
      } else {
        return { ...state, height: (state.height -= 20) };
      }
    case RESET_SIZE:
      return { ...state, width: 200, height: 200 };

    default:
      return state;
  }
};
