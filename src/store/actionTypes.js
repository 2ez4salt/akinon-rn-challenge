export const NEXT = "NEXT";
export const PREVIOUS = "PREVIOUS";
export const WIDTH_PLUS = "WIDTH_PLUS";
export const WIDTH_MINUS = "WIDTH_MINUS";
export const HEIGHT_PLUS = "HEIGHT_PLUS";
export const HEIGHT_MINUS = "HEIGHT_MINUS";
export const RESET_SIZE = "RESET_SIZE";
